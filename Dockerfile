FROM openjdk:11

RUN apt-get update && apt-get -y install  htop

ADD target/*.jar app.jar
CMD ["java", "-jar", "app.jar"]
