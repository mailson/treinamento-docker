package com.webpublico.treinamentodocker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TreinamentoDockerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TreinamentoDockerApplication.class, args);
	}

}
